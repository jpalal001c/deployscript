#!/usr/bin/env python
# 0! backup current codes, bkp-<date>.
# 1. clone from a public gitrepo (master) 
# 2. cp clone_directory /path/to/htdocs
# 3. profit
# import string, sys

import urllib, json
import os
import sys
import argparse

from time import mktime, gmtime, strftime, time
import datetime

# remember to easy_install GitPython
# http://gitpython.readthedocs.org/en/stable/
from git import Repo
join = os.path.join

default_deploy_dir = '/var/www/html'

# look up if github or bitbucket uri, try run git clone from uri directly  

def clone_master(git_url):
    
    release_dir=''
    curtime = strftime("%Y%m%d%H%M%S", gmtime())	 
    release_dir = '/releases/' + curtime
    os.system("mkdir -p %s" % release_dir)
    os.system("echo Created %s" % release_dir) 
    Repo.clone_from(git_url, release_dir)
    print "Clone master complete!" 
  
    return release_dir

def backup_files(target):
    tar_file = strftime("%Y%m%d%H%M", gmtime()) + '.tar'
    # os.system("cp %s %s" % (source target) )
    os.system("tar -cf %s %s" % ( tar_file, target ))
   
#def deploy_release(release_dir):       

def main(argv=None):
#    if argv is None:
#        argv = sys.argv
#        deploy_dir = default_deploy_dir
#    else:
       # get argv for folder to deploy to (/var/www?)
	parser = argparse.ArgumentParser()
	parser.add_argument('-d', '--dir')
	parser.add_argument('-r', '--repo')
	args = parser.parse_args()
	deploy_dir = args.dir
	git_uri = args.repo
		
	#backup current files in deploy_dir to /bkp/<date>.tar
	backup_files(deploy_dir)

	download_dir = clone_master(git_uri)

	os.system("echo Master downloaded to %s " % download_dir)
	os.system("echo Copying files from %s into %s" % ( download_dir, deploy_dir ))
	os.system("cp -Rf %s/* %s" % ( download_dir, deploy_dir ) )
    
if __name__ == '__main__':
    if os.geteuid() != 0:
        sys.exit("You need to have root privileges to run this script.\nPlease try again, this time using 'sudo'. Exiting.")
    else:
        sys.exit(main())
